﻿#region using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace Parameters
{
    class Program
    {
        void DoWork()
        {
            // TODO:
        }

        static void Main(string[] args)
        {
            try
            {
                Program prog = new Program();
                prog.DoWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
